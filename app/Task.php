<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
  protected $fillable = [
    'name',
    'project_id',
    'days',
    'hours',
    'user_id',
    'company_id'
  ];

  public function users()
  {
    return $this->belongsToMany(User::class);
  }

  public function project()
  {
    return $this->belongsTo(Project::class);
  }

  public function company()
  {
    return $this->belongsTo(Company::class);
  }
}
